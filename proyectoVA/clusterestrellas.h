#ifndef CLUSTERESTRELLAS_H
#define CLUSTERESTRELLAS_H

#include "estrella.h"

class clusterEstrellas
{
public:
    clusterEstrellas();
    clusterEstrellas(int estrellas, int masa, int radio, double tiempo);
    ~clusterEstrellas();
    QVector<estrella*> getEstrellas();
    void calcularVecinas();

private:
    int estrellas;
    int espacio;
    int masa;
    int radio;
    double tiempo;
    QVector<estrella*> cluster;

    //metodo
    void generarPosicionInicialEstrellas();



};

#endif // CLUSTERESTRELLAS_H
