#ifndef ESTRELLA_H
#define ESTRELLA_H

#include <QVector>

class estrella
{
public:
    estrella();
    estrella(int masa, int radio, double tiempo);
    ~estrella();

    //metodos publicos
    void setPosicionInicial(int x,int y, int z);
    QVector<double> getPosicionActual();
    void actualizarPosicionActual();
    void calcularVecinas(QVector <estrella*> &estrellas);

private:
    int masa;
    int radio;
    double tiempo;
    QVector<estrella*> vecinas;
    QVector<double> posicionActual;
    QVector<double> posicionNueva;
    QVector<double> velocidadActual;
    QVector<double> aceleracion;

    //metodos
    void calcularNuevaPosicion();

    //metodos operaciones vectoriales
    double normaVectorial(QVector<double> &a);
    QVector<double> restaVectores(QVector<double> &a, QVector<double> &b);
    QVector<double> sumaVectores(QVector<double> &a, QVector<double> &b);
    QVector<double> productoEscalar(double escalar, QVector<double> &a);



};

#endif // ESTRELLA_H
