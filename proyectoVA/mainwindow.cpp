#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //Modificando propiedades spinboxes.
    ui->tiempoSpin->setSingleStep(0.05);
    ui->tiempoSpin->setMinimum(0.01);
    ui->tiempoSpin->setValue(0.5);

    ui->estrellasSpin->setRange(0,500);
    ui->estrellasSpin->setSingleStep(10);
    ui->estrellasSpin->setValue(100);

    ui->masaSpin->setRange(0,100);
    ui->masaSpin->setValue(4);

    ui->radioSpin->setRange(0,100);
    ui->radioSpin->setValue(10);


    this->row=1;
    this->column=1;



}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::pintar()
{
    if(column==3 && row==3)
    {
        ui->playB->setEnabled(false);
    }
    grafica *vista = new grafica(ui->estrellasSpin->value(),
                              ui->masaSpin->value(),
                              ui->radioSpin->value(),
                              ui->tiempoSpin->value());

    if(column<=3)
    {
        this->vistas.append(vista);
        vista->show();
        column++;
    }else if (row<3) {
        this->vistas.append(vista);
        column=1;
        row++;
        vista->show();
        column++;
    }
}

void MainWindow::reiniciar()
{
    for(int i=0; i<vistas.size(); i++)
    {
        vistas.at(i)->close();
        delete vistas[i];
    }

    column=1;
    row=1;
    this->vistas.clear();

    ui->playB->setEnabled(true);
}


