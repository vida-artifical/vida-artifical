#ifndef GRAFICA_H
#define GRAFICA_H

#include <QGLWidget>
#include <GL/glu.h>

#include <QTimer>
#include <QKeyEvent>

#include <math.h>
#include "clusterestrellas.h"

class grafica : public QGLWidget
{
    Q_OBJECT

public:
    grafica();
    grafica(int estrellas, int masa, int radio, double tiempo);
    ~grafica();
private:
    //Atributos efectos movimientos espacio
    QTimer *m_timer;
    GLfloat anglex;
    GLfloat angley;
    GLfloat z;

    GLUquadric *quadObj;

    clusterEstrellas *cluster;

    void initializeGL();
    void resizeGL(int width, int height);
    void paintGL();

    void timeOut();

    void drawWireSphere(GLdouble radius, GLint slices, GLint stacks);
    void drawSolidSphere(GLdouble radius, GLint slices, GLint stacks);

protected slots:
    void timeOutSlot();
    void keyPressEvent(QKeyEvent *event);

};

#endif // GRAFICA_H
