#-------------------------------------------------
#
# Project created by QtCreator 2012-12-19T16:55:09
#
#-------------------------------------------------

QT       += core gui opengl

TARGET = proyectoVA
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    estrella.cpp \
    clusterestrellas.cpp \
    grafica.cpp

HEADERS  += mainwindow.h \
    estrella.h \
    clusterestrellas.h \
    grafica.h

FORMS    += mainwindow.ui

LIBS    += -lGLU
