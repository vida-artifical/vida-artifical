#include "estrella.h"
#include <math.h>


estrella::estrella()
{
}

estrella::estrella(int masa, int radio, double tiempo)
{
    this->masa= masa;
    this->radio= this->masa*radio;
    this->tiempo= tiempo;

    for(int i=0; i<3; i++)
    {
        this->velocidadActual.append(0.1);
        this->aceleracion.append(0.0);
    }
}

estrella::~estrella()
{
}


double estrella::normaVectorial(QVector<double> &a)
{
    int suma=0;
    for (int i=0;i<a.size();i++)
        suma+= a.at(i)*a.at(i);
    return sqrt(suma);
}

QVector<double> estrella::restaVectores(QVector<double> &a, QVector<double> &b)
{
    QVector<double> result;
    for (int i=0;i<a.size();i++){
        result.append(b.at(i)-a.at(i));
    }
    return result;
}


QVector<double> estrella::sumaVectores(QVector<double> &a, QVector<double> &b)
{
    QVector<double> result;
    for (int i=0;i<a.size();i++){
        result.append(b.at(i)+a.at(i));
    }
    return result;
}

QVector<double> estrella::productoEscalar(double escalar, QVector<double> &a)
{
    QVector<double> vectorEscalado;
    for (int i=0;i<a.size();i++){
        vectorEscalado.append(a.at(i)*escalar);
    }
    return vectorEscalado;
}


void estrella::setPosicionInicial(int x, int y, int z)
{
    this->posicionActual.append(x);
    this->posicionActual.append(y);
    this->posicionActual.append(z);

}

QVector<double> estrella::getPosicionActual()
{
    return this->posicionActual;
}

void estrella::calcularVecinas(QVector<estrella *> &estrellas)
{
    for(int i=0; i<estrellas.size();i++)
    {
        estrella *star = estrellas.at(i);
        QVector<double> posicion = star->getPosicionActual();
        QVector<double> resta= restaVectores(this->posicionActual, posicion);

        double norma= this->normaVectorial(resta);

        if(norma>0 && norma<this->radio)
        {
            vecinas.append(star);
        }
    }

   calcularNuevaPosicion();
}

void estrella::calcularNuevaPosicion()
{
    double gravedad= 0.0667;

    QVector<double> aceleracionNueva;
    for(int i=0; i<3; i++)
    {
        aceleracionNueva.append(0.0);
    }

    for(int i=0; i<this->vecinas.size(); i++)
    {
        estrella *star = this->vecinas.at(i);
        QVector<double> posicion= star->getPosicionActual();

        QVector<double> resta = this->restaVectores(this->posicionActual, posicion);

        double norma= this->normaVectorial(resta);
        //se toma epsilon como cero y se usa solo la norma de la posicion de la estrella.
        double operacionNorma= pow(norma,3.0/2.0);

        QVector <double> operacionEscalar= productoEscalar(this->masa/operacionNorma, resta);
        aceleracionNueva= sumaVectores(aceleracionNueva, operacionEscalar);
    }

    this->aceleracion= sumaVectores(aceleracionNueva, this->aceleracion);
    this->velocidadActual= productoEscalar(this->tiempo, this->aceleracion);

    QVector<double> nuevaPosicion = productoEscalar(this->tiempo*this->tiempo, this->aceleracion);
    QVector<double> tempPosicion= productoEscalar(this->tiempo, this->velocidadActual);
    nuevaPosicion = sumaVectores(nuevaPosicion, tempPosicion);
    nuevaPosicion = productoEscalar(gravedad, nuevaPosicion);
    nuevaPosicion = sumaVectores(nuevaPosicion, this->posicionActual);

    //Actualizar posicion nueva
    for(int i=0; i<3; i++)
    {
        this->posicionNueva.append(nuevaPosicion.at(i));
    }

}

void estrella::actualizarPosicionActual()
{
    this->posicionActual.clear();
    for(int i=0; i<this->posicionNueva.size(); i++)
    {
        posicionActual.append(this->posicionNueva.at(i));
    }

    vecinas.clear();
    posicionNueva.clear();

}
