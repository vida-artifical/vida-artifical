#include "grafica.h"

grafica::grafica()
{
}

grafica::grafica(int estrellas, int masa, int radio, double tiempo)
{
    this->m_timer = new QTimer(this);
    connect(m_timer,SIGNAL(timeout()), this, SLOT(timeOutSlot()));
    this->m_timer->start(100);

    //coordenadas donde inicia las graficas.

    this->anglex=0.0f;
    this->angley=0.0f;
    this->z= -15.0f;

    this->cluster = new clusterEstrellas(estrellas,masa,radio,tiempo);
}

grafica::~grafica()
{
    delete this->m_timer;
    delete this->cluster;
}

void grafica::initializeGL()
{
    glClearStencil(0x0);
    glEnable(GL_STENCIL_TEST);

    glShadeModel(GL_SMOOTH);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    quadObj = gluNewQuadric();
}

void grafica::resizeGL(int width, int height)
{
    if(height==0)
    {
        height=1;
    }

    glViewport(0,0,width,height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f,(GLfloat)width/(GLfloat)height, 0.1f, 100.0f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void grafica::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    glTranslatef(0.0f,0.0f,z);
    glRotated(anglex, 0.0f,1.0f,0.0f);
    glRotated(angley, 1.0f,0.0f,0.0f);

    glColor3f(1.0f,1.0f,0.0f);

    /*GLfloat fogColor[4]= {0.4f, 0.4f, 0.2f, 1.0f};
    glFogi(GL_FOG_MODE, GL_EXP2);
    glFogfv(GL_FOG_COLOR, fogColor);
    glFogf(GL_FOG_DENSITY, 0.35f);
    glHint(GL_FOG_HINT,  GL_NICEST);
    glFogf(GL_FOG_START, 1.0f);
    glFogf(GL_FOG_END, 5.0f);
    glEnable(GL_FOG);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);*/

    QVector<estrella*> estrellas = this->cluster->getEstrellas();

    for(int i=0; i<estrellas.size(); i++)
    {
        estrella *star = estrellas.at(i);

        QVector<double> posicion= star->getPosicionActual();
        GLdouble coorX=posicion.at(0)/15;
        GLdouble coorY=posicion.at(1)/15;
        GLdouble coorZ=posicion.at(2)/15;

        glPushMatrix();
            glTranslatef(-5.0f,-5.5f,-1.0f);
            glTranslatef(coorX,coorY,coorZ);
            drawSolidSphere(0.07,10,5);
            //drawWireSphere(0.07,10,5);
        glPopMatrix();
    }
    glFlush();
}

void grafica::timeOut()
{
    this->cluster->calcularVecinas();
    updateGL();
}

void grafica::timeOutSlot()
{
    this->timeOut();
}

void grafica::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Escape:
        z--;
        break;

    case Qt::Key_Left:
        anglex -= 0.7;
        break;
    case Qt::Key_Right:
        anglex += 0.7;
        break;
    case Qt::Key_Up:
        angley += 0.7;
        break;
    case Qt::Key_Down:
        angley -= 0.7;
        break;

   case Qt::Key_Minus :
        z++;
        break;

    case Qt::Key_Plus:
         z--;
         break;
    }
    updateGL();
}

void grafica::drawWireSphere(GLdouble radius, GLint slices, GLint stacks)
{
    gluQuadricDrawStyle(quadObj, GLU_LINE);
    gluQuadricNormals(quadObj, GLU_SMOOTH);
    gluSphere(quadObj, radius, slices, stacks);
}

void grafica::drawSolidSphere(GLdouble radius, GLint slices, GLint stacks)
{
    gluQuadricDrawStyle(quadObj, GLU_FILL);
    gluQuadricNormals(quadObj, GLU_SMOOTH);
    gluSphere(quadObj, radius, slices, stacks);
}


