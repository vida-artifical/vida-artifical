#include "clusterestrellas.h"
#include <time.h>

clusterEstrellas::clusterEstrellas()
{
}

clusterEstrellas::clusterEstrellas(int estrellas, int masa, int radio, double tiempo)
{
    this->espacio = 200;
    this->estrellas= estrellas;
    this->masa= masa;
    this->radio= radio;
    this->tiempo= tiempo;
    this->generarPosicionInicialEstrellas();
    this->calcularVecinas();
}

clusterEstrellas::~clusterEstrellas()
{
    for(int i=0; i<this->estrellas; i++)
    {
        delete this->cluster[i];
    }
}

void clusterEstrellas::generarPosicionInicialEstrellas()
{
    srand(time(NULL));

    for(int i=0; i<this->estrellas; i++)
    {
        //Coordenadas
        double aleatorio= (double) rand()/ (double) RAND_MAX;
        int coorX= (int) (aleatorio*this->espacio);

        aleatorio= (double) rand()/ (double) RAND_MAX;
        int coorY= (int) (aleatorio*this->espacio);

        aleatorio= (double) rand()/ (double) RAND_MAX;
        int coorZ= (int) (aleatorio*this->espacio);

        estrella *star = new estrella(this->masa,this->radio, this->tiempo);
        star->setPosicionInicial(coorX,coorY,coorZ);

        cluster.append(star);
    }
}


void clusterEstrellas::calcularVecinas()
{
    for(int i=0; i<this->estrellas; i++)
    {
        estrella *star = cluster.at(i);
        star->calcularVecinas(this->cluster);
    }

    for(int i=0; i<this->estrellas; i++)
    {
        estrella *star = cluster.at(i);
        star->actualizarPosicionActual();
    }
}

QVector<estrella*> clusterEstrellas::getEstrellas()
{
    return this->cluster;
}
