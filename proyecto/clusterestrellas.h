#ifndef CLUSTERESTRELLAS_H
#define CLUSTERESTRELLAS_H

#include "estrella.h"


class clusterEstrellas
{
    int n; //cantidad de estrellas
    int p; //tamaño del espacio p*p*p  x y z, entre 0 y p
    int masa;
    int radio;
    double tiempo;

    QVector <estrella> cluster;
public:
    clusterEstrellas(int estrellas, int masa, int radio, double tiempo);
    clusterEstrellas();
    ~clusterEstrellas();
    void generarPosInicialEstrellas();
    void calcularVecinas();
     QVector <estrella> getEstrellas();
     void setMasa(int nuevaMasa);
     void setRadio(int nuevoRadio);
     void setTiempo(double nuevoTiempo);
     void setEstrellas(int nuevaEstrellas);

};

#endif // CLUSTERESTRELLAS_H
