#ifndef ESTRELLA_H
#define ESTRELLA_H

#include <QVector>

class estrella
{

      int masa;
      int radio;
      double tiempo;
      QVector<estrella>  vecinas;
      QVector<double>  posicionActual;
      QVector<double>  posicionNueva;
      QVector <double> velocidadActual;
       QVector<double> aceleracion;
public:

     estrella(int masa, int radio, double tiempo);
     estrella();
     ~estrella();
     void calcularVecinas(QVector < estrella >  posEstrellas );
     void calcularNuevaPosion();
     void limpiarVecinas();
     void limpiarNuevaPosion();
     void setPosini(int x, int y , int z);
     QVector<double> getposActual();
     void actualizarPosActual();



     double normaVectorial(QVector<double>);
     QVector<double> restaVectores(QVector<double> a , QVector<double> b);
     QVector<double> sumaVectores(QVector<double> a , QVector<double> b);


     QVector<double>  productoEscalar(double escalar, QVector <double> vect);
     QVector<double>  productoEscalarInt(double escalar, QVector <double> vect);


};

#endif // ESTRELLA_H
