#include "superficie.h"
#include <QDebug>
#include<iostream>

Superficie::Superficie(int estrellas, int masa, int radio, double tiempo)
{
    m_timer = new QTimer(this);
    connect(m_timer,SIGNAL(timeout()), this, SLOT(timeOutSlot()));
    m_timer->start(100); //100 milisegundo
    anglex=0.0f;
    angley=0.0f;
    z=-15.0f;
    Lx=0.0;


    /*
    clusterIni = new clusterEstrellas(200,4,10,0.5);
    cluster = clusterIni;*/

   cluster.setEstrellas(estrellas);
    cluster.setMasa(masa);
    cluster.setRadio(radio);
    cluster.setTiempo(tiempo);
    cluster.generarPosInicialEstrellas();
    cluster.calcularVecinas();

    //cluster = new clusterEstrellas(200,4,10,0.5);

    std::cout<<"se crea superficio direccion cluster: "<<&cluster<<std::endl;
}

void Superficie::initializeGL()
{

    glClearStencil(0x0);
    glEnable(GL_STENCIL_TEST);

    glShadeModel(GL_SMOOTH);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);  // Enables Depth Testing
    glDepthFunc(GL_LEQUAL);  // The Type Of Depth Test To Do
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // Really Nice Perspective

    quadObj = gluNewQuadric();


}

void Superficie::resizeGL(int width, int height)
{
    if (height==0) // Prevent A Divide By Zero By
    {
        height=1;  // Making Height Equal One
    }

    glViewport(0, 0, width, height); // Reset The Current Viewport

    //The projection matrix is responsible for adding perspective to our scene
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity(); // Reset The Projection Matrix
    // Calculate The Aspect Ratio Of The Window
    gluPerspective(45.0f,(GLfloat)width/(GLfloat)height,0.1f,100.0f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void Superficie::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glLoadIdentity();

    glTranslatef(0.0f,0.0f,z);
    glRotated(anglex, 0.0f,1.0f,0.0f);
    glRotated(angley, 1.0f,0.0f,0.0f);


     glColor3f(1.0f,1.0f,1.0f);

     GLfloat fogColor[4]= {0.4f, 0.4f, 0.2f, 1.0f};
     glFogi(GL_FOG_MODE, GL_EXP2);
     //glFogi(GL_FOG_MODE, GL_EXP); //Tipos de neblina/humo
     //glFogi(GL_FOG_MODE, GL_LINEAR);
     glFogfv(GL_FOG_COLOR, fogColor);
     glFogf(GL_FOG_DENSITY, 0.35f);              // How Dense Will The Fog Be
     glHint(GL_FOG_HINT,  GL_NICEST);          // Fog Hint Value
     glFogf(GL_FOG_START, 1.0f);             // Fog Start Depth
     glFogf(GL_FOG_END, 5.0f);
     glEnable(GL_FOG);
     glEnable(GL_BLEND);
     glBlendFunc(GL_SRC_ALPHA, GL_ONE);



     glPushMatrix();
        glRotatef(Lx, 0.0f,1.0f,0.0f);
     glPopMatrix();







   //  std::cout<<"direccion vector estrellas cluster: "<<&(cluster.getEstrellas())<<std::endl;


   QVector <estrella> estrellas=  cluster.getEstrellas();
    //QVector <estrella> estrellas=  cluster->getEstrellas();

    std::cout<<"direccion vector estrellas creado : "<<&estrellas<<std::endl;

    for (int i=0;i<estrellas.size();i++)
    {
        estrella estre= estrellas.at(i);

        QVector <double > posxyz= estre.getposActual();

        GLdouble x1=posxyz.at(0)/15;
        GLdouble y2=posxyz.at(1)/15;
        GLdouble z2=-1*posxyz.at(2)/15;

        //std::cout<<"estrella "<<i<<"::"<<posxyz.at(0)<<";"<<posxyz.at(1)<<"\n";
        //std::cout<<"coordenada "<<x<<";"<<y<<";"<<z<<"\n";

        glPushMatrix();
            glTranslatef(-5.0f,-5.5f,-1.0f);
            glTranslatef(x1,y2,z2);
            drawSolidSphere(0.07,10,5);
            //drawWireSphere(0.07,10,5);
        glPopMatrix();

    }





    glFlush();
}

void Superficie::timeOut()
{
    //angle+=0.5f;

    std::cout<<"se hace repaint '' direccion cluster: "<<&cluster<<std::endl;
    cluster.calcularVecinas();
    //cluster->calcularVecinas();
    updateGL();

}

void Superficie::timeOutSlot()
{
    this->timeOut();
}

void Superficie::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Escape:
        //QCoreApplication::exit();
        z--;
        break;

    case Qt::Key_Left:
        anglex -= 0.7;
        break;
    case Qt::Key_Right:
        anglex += 0.7;
        break;
    case Qt::Key_Up:
        angley += 0.7;
        break;
    case Qt::Key_Down:
        angley -= 0.7;
        break;

   case Qt::Key_Minus :
        z++;
        break;

    case Qt::Key_Plus:
         z--;
         break;
    }
    updateGL();
}



void Superficie::drawWireSphere(GLdouble radius, GLint slices, GLint stacks)
{
    //glLineWidth(0.1);
    gluQuadricDrawStyle(quadObj, GLU_LINE);
    gluQuadricNormals(quadObj, GLU_SMOOTH);
    gluSphere(quadObj, radius, slices, stacks);
}

void Superficie::drawSolidSphere(GLdouble radius, GLint slices, GLint stacks)
{
    gluQuadricDrawStyle(quadObj, GLU_FILL);
    gluQuadricNormals(quadObj, GLU_SMOOTH);

    gluSphere(quadObj, radius, slices, stacks);
}




void Superficie::mousePressEvent(QMouseEvent *event)
{
    switch (event->button()) {
    case Qt::LeftButton:
        Lx += 20.0f;
        updateGL();
        break;

    case Qt::MidButton:
    case Qt::RightButton:
        Lx -= 20.0f;
        updateGL();
        break;
    default:
        break;
    }
}



