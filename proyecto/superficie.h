#ifndef SUPERFICIE_H
#define SUPERFICIE_H

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

//#include <QtOpenGL>
#include <QGLWidget>
#include <QTimer>
#include <QKeyEvent>
#include <math.h>
#include "clusterestrellas.h"
#include <GL/glu.h>

class Superficie : public QGLWidget
{
    Q_OBJECT


public:
    Superficie(int estrellas, int masa, int radio, double tiempo);
    Superficie();

    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

    void timeOut();

protected slots:
    void timeOutSlot();
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);

private:
    QTimer *m_timer;
    GLfloat anglex;
    GLfloat angley;
    GLUquadricObj *quadObj;

    void drawWireSphere(GLdouble radius, GLint slices, GLint stacks);
    void drawSolidSphere(GLdouble radius, GLint slices, GLint stacks);



    GLfloat z;
    GLfloat Lx;

    clusterEstrellas cluster;

};

#endif // SUPERFICIE_H
