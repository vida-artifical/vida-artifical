#-------------------------------------------------
#
# Project created by QtCreator 2012-01-20T14:59:50
#
#-------------------------------------------------

QT       += opengl

SOURCES += main.cpp \
    superficie.cpp \
    estrella.cpp \
    clusterestrellas.cpp

HEADERS  += \
    superficie.h \
    estrella.h \
    clusterestrellas.h

RESOURCES += \
    images.qrc

LIBS += -lGLU

FORMS += \
    mainwindow.ui
