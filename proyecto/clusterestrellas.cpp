#include "clusterestrellas.h"
#include <QDebug>
#include <time.h>
#include <iostream>

clusterEstrellas::clusterEstrellas(int estrellas, int masa, int radio, double tiempo)
{
    n=estrellas;
    p=200;
    generarPosInicialEstrellas();
    calcularVecinas();
    this->masa= masa;
    this->radio= radio;
    this->tiempo= tiempo;


}
clusterEstrellas::clusterEstrellas(){
    p=200;
}

clusterEstrellas::~clusterEstrellas(){}

void clusterEstrellas::generarPosInicialEstrellas()

{

    srand ( time(NULL) );// semilla hora

    for (int i=0;i<n;i++){//generar aleatoriamente la posicion inicial para cada estrella

        double aleatorio= (double ) rand()/ (double) RAND_MAX;// valor entre 0-1
        int coordenadaX= (int) (aleatorio*p);

        aleatorio= (double ) rand()/ (double) RAND_MAX;// valor entre 0-1
        int coordenadaY= (int) (aleatorio*p);

        aleatorio= (double ) rand()/ (double) RAND_MAX;// valor entre 0-1
        int coordenadaZ= (int) (aleatorio*p);

        estrella *estre= new estrella(masa,radio,tiempo);
        estre->setPosini(coordenadaX,coordenadaY,coordenadaZ);


        //qDebug()<<"Date:"<<coordenadaX;
       // std::cout<<coordenadaX<<"-"<<coordenadaY <<"-"<< coordenadaZ<<"\n";


        cluster.append(*estre);

    }

}

void clusterEstrellas::calcularVecinas(){


   std::cout<<"Entrea a calcular vecinas: " <<std::endl;

    for (int i=0;i<cluster.size();i++)
    {
    estrella &estre= cluster[i];
    if(i==0){std::cout<<"estrella para calcular vecinas dir: " <<&estre<<std::endl;}

    estre.calcularVecinas(cluster);// dentro de calcular vecinas al final hace un llamado a calcularNuevaPosion ahi mismo esta la referencia donde explican
    // la formula usada  tenemos guardado la posicion en una variable llamada posicionNUeva pues no podemos actualizar el
    //tablero hasta que todas lo hayan hecho
    }

    //-------------------------actualizar posActual
    for (int i=0;i<cluster.size();i++)
    {
        estrella &estre= cluster[i];
        if(i==0){std::cout<<"estrella para calcular vecinas dir: " <<&estre<<std::endl;}

    estre.actualizarPosActual();
    }


}

QVector <estrella> clusterEstrellas::getEstrellas(){


    return cluster;
}

void clusterEstrellas::setEstrellas(int nuevaEstrellas){
    this->n= nuevaEstrellas;
}

void clusterEstrellas::setMasa(int nuevaMasa){
    this->masa= nuevaMasa;
}

void clusterEstrellas::setRadio(int nuevoRadio){
    this->radio= nuevoRadio;
}

void clusterEstrellas::setTiempo(double nuevoTiempo){
    this->tiempo= nuevoTiempo;
}
