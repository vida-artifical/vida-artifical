#include "estrella.h"
#include <QDebug>
#include <iostream>
#include <math.h>

estrella::estrella(int masa, int radio, double tiempo)
{

    this->masa=masa;
    this->radio=masa*radio;
    this->tiempo= tiempo;
    velocidadActual.append(0.1);
    velocidadActual.append(0.1);
    velocidadActual.append(0.1);

    aceleracion.append(0.0);
    aceleracion.append(0.0);
    aceleracion.append(0.0);



}

estrella::estrella() {}
estrella::~estrella(){}


void estrella::setPosini(int x, int y , int z){

    posicionActual.append(x);
    posicionActual.append(y);
    posicionActual.append(z);


}

QVector<double> estrella::getposActual(){


    return posicionActual;


}

void estrella::calcularVecinas(QVector < estrella >  posEstrellas ){


    //std::cout<<posicionActual.at(0)<<"-"<<posicionActual.at(1) <<"-"<< posicionActual.at(2)<<"\n";



    //std::cout<<"norma::" <<normaVectorial(posicionActual)<<"\n";

    for (int i=0;i<posEstrellas.size();i++){
        estrella estre= posEstrellas.at(i);

        if(i==0){std::cout<<"estrella para calcular vecinas en estrella dir: " <<&estre<<std::endl;}



        QVector <double> resta = restaVectores(posicionActual,estre.getposActual() );



        //std::cout<<resta.at(0)<<"-"<<resta.at(1) <<"-"<< resta.at(2)<<"\n";

        double norma= normaVectorial(resta);


        if(norma>0 && norma<radio ){
            //std::cout<<"norma resta ::" <<norma<<"\n";
            vecinas.append(estre);
        }




    }


    calcularNuevaPosion();//una vez calculamos las vecinas podemos saber que fuerzas intervienen

}


void estrella::calcularNuevaPosion(){

    //aceleracion http://stackoverflow.com/questions/9523928/n-body-simulation-how-to-choose-the-softening-factor
    // por ahora pensemos que todas las masas son iguales
    // las vecinas osea las que inciden se encuentran en el vector vecinas




    //double tiempo= 0.3;
    double gravedad=0.0667;// la pase de metros cubicos a km cubicos


    QVector <double> aceleracionNueva;
    aceleracionNueva.append(0.0);
    aceleracionNueva.append(0.0);
    aceleracionNueva.append(0.0);
    for (int i=0;i<vecinas.size();i++){


        estrella estre= vecinas.at(i);
        QVector <double> resta = restaVectores(posicionActual,estre.getposActual() );



        double  norma = normaVectorial(resta);

        double normaEpsilon= norma ;

        //empircamente tomamos un epsilon = 1
        double escalar =pow(normaEpsilon,3/2);

        QVector <double> escalado=productoEscalarInt(masa/escalar,resta);
        qDebug()<<"vecotr escalado" << escalado<< resta;


        std::cout<<"norma::"<<norma<< " escalar::"<<escalar<<"\n";


        aceleracionNueva=sumaVectores(aceleracionNueva,escalado);

    }

    //std::cout<<"aceleracion::"<<aceleracion.at(0)<<"-"<<aceleracion.at(1)<< "-"<<aceleracion.at(2)<<"\n";


    aceleracion=sumaVectores(aceleracionNueva,aceleracion);// debido a que el cuerpo lleva una aceleracion anterior esta se le suma a la nueva
    QVector <double> nuevapos= productoEscalar(tiempo*tiempo,aceleracion);



    velocidadActual= productoEscalar(tiempo,aceleracion);// la velocidad cambia  de acuerdo a la aceleracion en el tiempo.


    nuevapos= sumaVectores(nuevapos,productoEscalar(tiempo,velocidadActual));


    nuevapos=productoEscalar(gravedad,nuevapos);



    nuevapos=sumaVectores(nuevapos,posicionActual);



   // std::cout<<"posActual::"<<posicionActual.at(0)<<"-"<<posicionActual.at(1)<< "-"<<posicionActual.at(2)<<" VEcinas"<<vecinas.size()<<"\n";


    //std::cout<<"cambio de pos::"<<nuevapos.at(0)<<"-"<<nuevapos.at(1)<< "-"<<nuevapos.at(2)<<" VEcinas"<<vecinas.size()<<"\n";


    posicionNueva.append(nuevapos.at(0));
    posicionNueva.append(nuevapos.at(1));
    posicionNueva.append(nuevapos.at(2));



//    std::cout<<"gravedad cambio de pos::"<<posicionNueva.at(0)<<"-"<<posicionNueva.at(1)<< "-"<<posicionNueva.at(2)<<" size "<<posicionNueva.size()<<"\n";

    //std::cout<<"operando::"<<gravedad*1.0<<"\n";



}

void estrella::actualizarPosActual(){


    //std::cout<<"entro a actualizar"<<"\n";

    //std::cout<<"posActual::"<<posicionActual.at(0)<<"-"<<posicionActual.at(1)<< "-"<<posicionActual.at(2)<<"\n";


    //vecinas.clear();

    if(posicionNueva.size()<3) {
        std::cout<<" condicion posicion nueva size"<<posicionNueva.size()<<"\n";
        std::cout<<"condicion posicion vecinas size"<<vecinas.size()<<"\n";

        return;

    }

    posicionActual.clear();
    posicionActual.append(posicionNueva.at(0));
    posicionActual.append(posicionNueva.at(1));
    posicionActual.append(posicionNueva.at(2));

    vecinas.clear();

    //std::cout<<"posActualizada::"<<posicionActual.at(0)<<"-"<<posicionActual.at(1)<< "-"<<posicionActual.at(2)<<"\n";


    //std::cout<<"x::"<<posicionActual.at(0)<<"y::"<<posicionActual.at(1)<<"vecinas"<<vecinas.size()<<"\n";
    posicionNueva.clear();// limpiar todo para la siguiente iteraccion



}




double estrella::normaVectorial(QVector<double> a){

    int suma=0;

    for (int i=0;i<a.size();i++)
        suma+= a.at(i)*a.at(i);


    return sqrt(suma);


}

QVector<double> estrella::restaVectores(QVector<double> a , QVector<double> b){

    QVector<double> result;

    for (int i=0;i<a.size();i++){
        result.append(b.at(i)-a.at(i));
    }

    return result;


}



QVector<double> estrella::sumaVectores(QVector<double> a , QVector<double> b){

    QVector<double> result;

    for (int i=0;i<a.size();i++){
        result.append(b.at(i)+a.at(i));
    }

    return result;


}




QVector<double> estrella::productoEscalar(double escalar, QVector <double> vect){



    QVector<double> vectorEscalado;

    vectorEscalado.append(vect.at(0)*escalar);
    vectorEscalado.append(vect.at(1)*escalar);
    vectorEscalado.append(vect.at(2)*escalar);


    return vectorEscalado;


}

QVector<double>  estrella::productoEscalarInt(double escalar, QVector <double> vect){



    QVector<double> vectorEscalado;

    vectorEscalado.append(vect.at(0)*escalar);
    vectorEscalado.append(vect.at(1)*escalar);
    vectorEscalado.append(vect.at(2)*escalar);


    return vectorEscalado;

}






